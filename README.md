# Crush
A template application that shows my take on a potential dating app. Basic, but has custom animations and app components that I find to be pretty and easy to interact with.

## Installation
Download the source code on your machine. Connect your Android device and run the app to get a debug option. You can also just take the APK directly off of the repo and install that instead.

### Notes
* This app was built for the latest API level 29
* App was optimized for a Pixel 3 XL. In a production level app I would aim to ensure that most Android sizes and Android versions were supported as much as possible. Or at the very least, ensure that no crashes or unexpected behaviors happened in lower Android versions.