package com.appme.crush

import android.animation.Animator

/**
 * Dummy implementation of animation listener
 */
abstract class AnimatorCallback : Animator.AnimatorListener {

    override fun onAnimationRepeat(animator: Animator?) {}
    override fun onAnimationEnd(animator: Animator?) {}
    override fun onAnimationCancel(animator: Animator?) {}
    override fun onAnimationStart(animator: Animator?) {}

}