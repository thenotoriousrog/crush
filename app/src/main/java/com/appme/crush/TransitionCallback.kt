package com.appme.crush

import android.transition.Transition


/**
 * Dummy implementations
 */
abstract class TransitionCallback : Transition.TransitionListener {

    override fun onTransitionEnd(transition: Transition) {}
    override fun onTransitionResume(transition: Transition) {}
    override fun onTransitionPause(transition: Transition) {}
    override fun onTransitionCancel(transition: Transition) {}
    override fun onTransitionStart(transition: Transition) {}
}