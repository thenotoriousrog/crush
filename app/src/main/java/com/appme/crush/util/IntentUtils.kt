package com.appme.crush.util

import android.content.Intent

object IntentUtils {
    const val TEXT_SIZE = "textSize"
    const val PADDING = "padding"
    const val SELECTED_ITEM_POSITION = "selectedItemPosition"

    fun hasAll(intent: Intent, vararg extras: String): Boolean {
        extras.forEach { extra ->
            if(!intent.hasExtra(extra)) return false
        }

        return true
    }

    fun hasAny(intent: Intent, vararg extras: String): Boolean {
        extras.forEach {  extra ->
            if(intent.hasExtra(extra)) return true
        }

        return false
    }

}