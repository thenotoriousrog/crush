package com.appme.crush.userprofile

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Rect
import android.graphics.drawable.Animatable2
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.*
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.appme.crush.*
import com.appme.crush.backend.User
import com.appme.crush.util.IntentUtils
import com.appme.crush.util.load
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textview.MaterialTextView

class UserProfileActivity : BaseActivity(), UserProfileActivityUpdater {

    companion object {

        private const val EXTRA_USER_ITEM = "extraUserItem"
        private const val EXTRA_PROFILE_PICTURE_TRANSITION_NAME = "profilePictureTransitionName"
        private const val EXTRA_NAME_TEXT_TRANSITION_NAME = "userNameTextTransitionName"

        /**
         * Generates a new intent with all proper bundle elements for this activity to be started with
         */
        fun newIntent(context: Context,
                      user: User,
                      profilePictureTransitionName: String?,
                      nameTextViewTransitionName: String?,
                      position: Int,
                      startTextSize: Float,
                      startTextViewPadding: Rect): Intent { // need to add the bottom app bar too!

            val intent = Intent(context, UserProfileActivity::class.java)
            intent.putExtra(EXTRA_USER_ITEM, user)
            intent.putExtra(EXTRA_PROFILE_PICTURE_TRANSITION_NAME, profilePictureTransitionName)
            intent.putExtra(EXTRA_NAME_TEXT_TRANSITION_NAME, nameTextViewTransitionName)

            intent.putExtra(IntentUtils.SELECTED_ITEM_POSITION, position)
            intent.putExtra(IntentUtils.TEXT_SIZE, startTextSize)
            intent.putExtra(IntentUtils.PADDING, startTextViewPadding)

            return intent
        }
    }

    private var isPlayingExitAnimation = false // tells the app that the user is leaving the activity
    private lateinit var profilePictureImageView: ImageView
    private lateinit var nameTextView: MaterialTextView

    private lateinit var appBarContainer: CoordinatorLayout
    private lateinit var appBar: BottomAppBar
    private lateinit var fab: FloatingActionButton
    private lateinit var crushIcon: ImageView
    private lateinit var interestChipGroup: ChipGroup

    override val layoutRes: Int
        get() = R.layout.user_profile_activity_layout

    override val snackBarContainer: View
        get() = fab

    private var isFillingCrushIcon = true

    private fun applyWindowFade() {
        val fade = Fade()
        fade.excludeTarget(android.R.id.statusBarBackground,true);
        fade.excludeTarget(android.R.id.navigationBarBackground,true);
        fade.excludeTarget(R.id.app_bar_container, true)
        fade.duration = 100L

        window.enterTransition = fade
        window.exitTransition = fade
    }

    private val crushStartAnimation by lazy { AnimationUtils.loadAnimation(this, R.anim.scale_up) }
    private val crushEndAnimation by lazy { AnimationUtils.loadAnimation(this, R.anim.scale_down) }
    private var user: User? = null

    private fun addInterestChips(user: User?) {

        user?.interests?.forEach { interest ->
            val chip = Chip(this).apply {
                text = interest
                chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                setTextColor(ContextCompat.getColor(context, R.color.white))
            }

            val scaleUpAnim = AnimationUtils.loadAnimation(this, R.anim.scale_up)
            interestChipGroup.addView(chip)
            chip.startAnimation(scaleUpAnim)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        supportPostponeEnterTransition()

        window.sharedElementsUseOverlay = true
        super.onCreate(savedInstanceState)

        val extras = intent.extras
        user = extras?.getSerializable(EXTRA_USER_ITEM) as? User

        this.profilePictureImageView = findViewById(R.id.profile_picture)
        this.appBar = findViewById(R.id.bottom_app_bar)
        this.appBarContainer = findViewById(R.id.user_profile_app_bar_container)
        this.fab = findViewById(R.id.user_profile_fab)
        fab.setOnClickListener {
            super.displaySnackBarMessage(R.string.feature_not_available, Snackbar.LENGTH_SHORT)
        }

        appBarContainer.translationY = appBarContainer.height.toFloat()
        this.crushIcon = findViewById(R.id.crush_icon)
        this.interestChipGroup = findViewById(R.id.user_interests)

        crushIcon.setOnClickListener { crushIcon ->
            val icon = crushIcon as ImageView
            val crushIconAnimatable = crushIcon.drawable as Animatable2
            crushIconAnimatable.start()
            crushIconAnimatable.registerAnimationCallback(object : Animatable2.AnimationCallback() {
                override fun onAnimationEnd(drawable: Drawable?) {
                    super.onAnimationEnd(drawable)
                    if(isFillingCrushIcon) {
                        isFillingCrushIcon = false
                        icon.setImageResource(R.drawable.heart_break_anim)
                    } else {
                        isFillingCrushIcon = true
                        icon.setImageResource(R.drawable.filled_heart_anim)
                    }
                }
            })
        }

        applyWindowFade()

        this.nameTextView = findViewById(R.id.name_text)

        val transitionName = extras?.getString(EXTRA_PROFILE_PICTURE_TRANSITION_NAME)
        profilePictureImageView.transitionName = transitionName
        nameTextView.transitionName = extras?.getString(EXTRA_NAME_TEXT_TRANSITION_NAME)

        val nameText = user?.name + ", " + user?.age
        nameTextView.text = nameText

        val set = TransitionSet()
        set.ordering = TransitionSet.ORDERING_TOGETHER

        val changeBounds = ChangeBounds()
        changeBounds.addTarget(profilePictureImageView)
        changeBounds.duration = 200L

        changeBounds.addListener(object : TransitionCallback() {
            override fun onTransitionStart(transition: Transition) {
                super.onTransitionStart(transition)
                appBarContainer.translationY = appBarContainer.height.toFloat()
                crushIcon.visibility = View.INVISIBLE
            }

            override fun onTransitionEnd(transition: Transition) {
                super.onTransitionEnd(transition)
                appBarContainer
                    .animate()
                    .translationY(0.0f)
                    .setDuration(200L)
                    .start()

                crushIcon.visibility = View.VISIBLE
                crushIcon.startAnimation(crushStartAnimation)
                addInterestChips(user)
            }
        })

        set.addTransition(changeBounds)

        window.sharedElementEnterTransition = set

        profilePictureImageView.load(user?.profilePicture) {
            supportStartPostponedEnterTransition()
        }
    }

    private fun triggerBackPress() {
        if(!isDestroyed) {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        this.isPlayingExitAnimation = true

        crushIcon.visibility = View.INVISIBLE
        crushIcon.startAnimation(crushEndAnimation)
        appBarContainer
            .animate()
            .translationY(appBarContainer.height.toFloat())
            .setDuration(200L)
            .setListener(object : AnimatorCallback() {
                override fun onAnimationEnd(animator: Animator?) {
                    super.onAnimationEnd(animator)
                    this@UserProfileActivity.triggerBackPress()
                }
            })
            .start()
    }
}