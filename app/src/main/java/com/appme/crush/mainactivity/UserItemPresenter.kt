package com.appme.crush.mainactivity

import com.appme.crush.backend.User

class UserItemPresenter(private var users: List<User>) {

    fun updateItems(users: List<User>) {
        this.users = users
    }

    fun onBindUserItemAtPosition(updater: UserItemUpdater, position: Int) {
        val user = users[position]
        updater.setUserName(user.name)
        updater.setUserProfilePic(user.profilePicture)
    }

    fun getUserAtPosition(position: Int): User {
        return users[position]
    }

    val userItemCount: Int
        get() = this.users.size
}