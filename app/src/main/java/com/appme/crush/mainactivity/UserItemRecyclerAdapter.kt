package com.appme.crush.mainactivity

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.appme.crush.R
import com.appme.crush.backend.User
import com.google.android.material.textview.MaterialTextView

/**
 * Reports item clicked to calling class
 */
typealias OnUserItemClickListener = (userItemPosition: Int,
                                     user: User,
                                     profilePictureImageView: ImageView,
                                     userNameTextView: MaterialTextView) -> Unit

class UserItemRecyclerAdapter(private val presenter: UserItemPresenter,
                              private val onUserItemClickListener: OnUserItemClickListener? = null) : RecyclerView.Adapter<UserViewHolder>() {

    private lateinit var lastViewHolderCreated: UserViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val viewHolder = UserViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false))
        this.lastViewHolderCreated = viewHolder
        return viewHolder
    }

    override fun getItemCount(): Int {
        return presenter.userItemCount
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        presenter.onBindUserItemAtPosition(holder, position)
        val user = presenter.getUserAtPosition(position)
        ViewCompat.setTransitionName(holder.profilePictureImageView, user.name + "profilePicture")
        ViewCompat.setTransitionName(holder.nameTextView, user.name)

        holder.itemView.setOnClickListener {
            onUserItemClickListener?.invoke(holder.adapterPosition, user, holder.profilePictureImageView, holder.nameTextView)
        }
    }
}