package com.appme.crush.mainactivity

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.appme.crush.R
import com.appme.crush.util.load
import com.bumptech.glide.Glide
import com.google.android.material.textview.MaterialTextView

class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), UserItemUpdater {

    val nameTextView: MaterialTextView = itemView.findViewById(R.id.user_name_text)
    val profilePictureImageView: ImageView = itemView.findViewById(R.id.user_profile_picture)


    override fun setUserName(name: String) {
        nameTextView.text = name
    }

    override fun setUserProfilePic(url: String) {
        profilePictureImageView.load(url)
    }
}