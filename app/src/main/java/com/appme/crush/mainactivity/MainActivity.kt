package com.appme.crush.mainactivity

import android.app.SharedElementCallback
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appme.crush.*
import com.appme.crush.backend.User
import com.appme.crush.userprofile.UserProfileActivity
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import java.io.InputStream
import androidx.core.util.Pair
import com.google.android.material.snackbar.Snackbar

class MainActivity : BaseActivity(), MainActivityUpdater {

    private lateinit var container: ConstraintLayout
    private lateinit var crushesRecyclerView: RecyclerView
    private lateinit var appBarContainer: CoordinatorLayout
    private lateinit var bottomAppBar: BottomAppBar
    private lateinit var fab: FloatingActionButton
    private lateinit var recyclerAdapter: UserItemRecyclerAdapter
    private lateinit var recyclerPresenter: UserItemPresenter

    private val presenter = MainActivityPresenter(this)

    override val layoutRes: Int
        get() = R.layout.activity_main

    override val snackBarContainer: View
        get() = fab

    private val exitSharedElementCallback = object : SharedElementCallback() {

        override fun onSharedElementEnd(
            sharedElementNames: MutableList<String>?,
            sharedElements: MutableList<View>?,
            sharedElementSnapshots: MutableList<View>?
        ) {
            super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots)
            appBarContainer
                .animate()
                .translationY(0.0f)
                .setDuration(200L)
                .start()

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        window.sharedElementsUseOverlay = true
        super.onCreate(savedInstanceState)

        setExitSharedElementCallback(exitSharedElementCallback)

        this.container = findViewById(R.id.container)
        this.crushesRecyclerView = findViewById(R.id.crushes_recycler_view)
        this.appBarContainer = findViewById(R.id.app_bar_container)
        this.bottomAppBar = findViewById(R.id.bottom_app_bar)
        this.fab = findViewById(R.id.home_fab)
        fab.setOnClickListener {
            super.displaySnackBarMessage(R.string.feature_not_available, Snackbar.LENGTH_SHORT)
        }

        this.recyclerPresenter = UserItemPresenter(ArrayList())
        this.recyclerAdapter = UserItemRecyclerAdapter(recyclerPresenter) { userItemPosition: Int, user: User, profilePictureImageView: ImageView, nameTextView: MaterialTextView ->

            val intent = UserProfileActivity.newIntent(
                this,
                user,
                ViewCompat.getTransitionName(profilePictureImageView),
                ViewCompat.getTransitionName(nameTextView),
                presenter.lastItemSelectedPosition,
                nameTextView.textSize,
                Rect(nameTextView.paddingLeft, nameTextView.paddingTop, nameTextView.paddingRight, nameTextView.paddingBottom))

            val profilePicturePair = Pair<View, String>(profilePictureImageView, ViewCompat.getTransitionName(profilePictureImageView) ?: "")

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                profilePicturePair)

            appBarContainer
                .animate()
                .translationY(appBarContainer.height.toFloat())
                .setDuration(200L)
                .start()

            startActivity(intent, options.toBundle())

        }

        this.crushesRecyclerView.adapter = recyclerAdapter
        crushesRecyclerView.layoutManager = GridLayoutManager(this, 2)

        crushesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy < 0) {
                    bottomAppBar.performShow()
                }

                val gridLayoutManager: GridLayoutManager = recyclerView.layoutManager as GridLayoutManager
                val lastItemVisiblePosition = gridLayoutManager.findLastCompletelyVisibleItemPosition()
                if(lastItemVisiblePosition == recyclerPresenter.userItemCount - 1) {
                    bottomAppBar.performHide()
                }
            }
        })

        presenter.fetchUsers()
    }

    override fun getRawResource(rawFile: Int): InputStream {
        return resources.openRawResource(rawFile)
    }

    override fun updateUsers(users: List<User>) {
        recyclerPresenter.updateItems(users)
        recyclerAdapter.notifyDataSetChanged()
    }

}
