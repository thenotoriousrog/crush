package com.appme.crush.mainactivity

import com.appme.crush.R
import com.appme.crush.backend.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber
import java.io.IOException

class MainActivityPresenter(private val updater: MainActivityUpdater) {

    private val gson = Gson()
    private val usersJson: String? by lazy {
        try {
            updater.getRawResource(R.raw.crushes).bufferedReader().use { it.readText() }
        } catch (exception: IOException) {
            Timber.e(exception, "Caught exception while attempting to parse users file")
            null
        }
    }
    private val userType = object : TypeToken<List<User>>() {}.type
    private lateinit var userItems: List<User>
    private var lastSelectedUserPosition = -1

    fun fetchUsers() {
        this.userItems = gson.fromJson(usersJson, userType)
        updater.updateUsers(userItems)
    }

    val lastItemSelectedPosition
        get() = this.lastSelectedUserPosition

}