package com.appme.crush.mainactivity

import androidx.annotation.RawRes
import com.appme.crush.backend.User
import java.io.InputStream

interface MainActivityUpdater {

    fun getRawResource(@RawRes rawFile: Int): InputStream

    fun updateUsers(users: List<User>)
}