package com.appme.crush.mainactivity

interface UserItemUpdater {

    fun setUserName(name: String)

    fun setUserProfilePic(url: String)
}