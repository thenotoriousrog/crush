package com.appme.crush

import android.app.Application
import timber.log.Timber

class CrushApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}