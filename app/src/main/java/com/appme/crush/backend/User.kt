package com.appme.crush.backend

import java.io.Serializable

data class User(val name: String,
                val age: Int,
                val profilePicture: String,
                val interests: List<String>) : Serializable {
    override fun toString(): String {
        return """
            name = $name,
            age = $age,
            profilePicture = $profilePicture,
        """.trimIndent()
    }
}