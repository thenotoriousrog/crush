package com.appme.crush

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity : AppCompatActivity() {

    /**
     * The layout file for the activity
     */
    abstract val layoutRes: Int

    /**
     * The view we want the snackbar container to pop up from
     */
    abstract val snackBarContainer: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
    }

    /**
     * Displays a snackBar message to the user
     * @param message - the message content
     * @param length - how long the message should show up
     */
    fun displaySnackBarMessage(@StringRes message: Int, length: Int) {
        val snackBar = Snackbar.make(snackBarContainer, message, length)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorComplimentary))
        snackBar.anchorView = snackBarContainer
        snackBar.show()
    }

}